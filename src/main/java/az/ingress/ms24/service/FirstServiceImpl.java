package az.ingress.ms24.service;

import az.ingress.ms24.repo.HelloEntity;
import az.ingress.ms24.repo.HelloRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class FirstServiceImpl implements FirstService {

    private final HelloRepo helloRepo;

    @Override
    public HelloEntity sayHello() {
        log.info("Application startup completed");
        HelloEntity helloEntity = new HelloEntity();
       // helloEntity.setId(1L);
        helloEntity.setMessage("Hello world from db");
        helloRepo.save(helloEntity);
        return helloEntity;
    }
}
