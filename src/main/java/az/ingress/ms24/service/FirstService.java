package az.ingress.ms24.service;

import az.ingress.ms24.repo.HelloEntity;

public interface FirstService {
    HelloEntity sayHello();

}
