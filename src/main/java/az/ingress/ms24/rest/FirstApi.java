package az.ingress.ms24.rest;

import az.ingress.ms24.repo.HelloEntity;
import az.ingress.ms24.service.FirstService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ms24")
public class FirstApi {

    private final FirstService firstService;

    public FirstApi(FirstService firstService) {
        this.firstService = firstService;
    }


    @GetMapping("/hello1")
    public HelloEntity sayHello() {
        return firstService.sayHello();
    }

    @GetMapping("/hello2")
    public String sayHello1() {
        return "Hello World 2";
    }
}
