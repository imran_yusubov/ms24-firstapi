package az.ingress.ms24.repo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface HelloRepo extends JpaRepository<HelloEntity, Long> {
}
