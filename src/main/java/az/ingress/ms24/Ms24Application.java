package az.ingress.ms24;

import az.ingress.ms24.repo.HelloEntity;
import az.ingress.ms24.repo.HelloRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@RequiredArgsConstructor
@SpringBootApplication
public class Ms24Application implements CommandLineRunner {

    private final HelloRepo helloRepo;

    public static void main(String[] args) {
        SpringApplication.run(Ms24Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Application startup completed");
        HelloEntity helloEntity = new HelloEntity();
        helloEntity.setId(1L);
        helloEntity.setMessage("Hello world from db");
        helloRepo.save(helloEntity);
    }
}
